# Contributing

The book you just read was built entirely on volunteer contributions from students at RIT, just like you.  What makes this book so great is the diversity of experiences that it can collect, but to do that, we need **your** perspective.  Any and all trans students at RIT are invited to participate in whatever way you can.  All you need to do is type a page or a paragraph or a sentence or a correction that you think this book could benefit from and submit it.  No contribution is too small, nor any experience insignificant, nor any student unqualified.

If you are already familiar with the open source contribution process you are may want to skip to the [contributor agreements](#contributor-agreements) section.

## Ways to Contribute

This book is built using software called `git`.  `git` is designed to provide version histories, reversions, forks, and more for any text-based document.  Even though it's commonly used in software, we've chosen to use `git` to create this book.  **However**, because we want *anyone* to be able to contribute, so we've made sure that multiple options are available, and we've also included [a tutorial on how to navigate GitLab](#how-to-use-gitlab) (where this book is hosted).

### Submit a Merge Request

The preferred way to submit a change is by using a Merge Request.  This work flow will be familiar to anyone who has worked in open source.  In a nutshell, submitting a merge request means making a change to the repository and then submitting the change for approval (being "merged in" to the repository).  You'll have an opportunity to discuss the change with the project custodians and any other students in a public forum.

This is the preferred way to submit a contribution, since it give you the most control over your submission (where it will be placed, how the other content will be worked into it, and what it will eventually look like), and leaves little work for the project custodians.  However, it's also a more complicated process, requiring a couple steps before you can get writing.  If you'd like to contribute this way, please see the ["How to use GitLab"](#how-to-use-gitlab) section for a brief tutorial, or if you're already familiar with the process, just read the [contributor agreements](#contributor-agreements) section and jump right in.

### Submit an Issue

There's a few reasons that a merge request might not be for you.  Perhaps it's just a bit too complicated, or maybe you don't know the specific change you want to make, but you'd like to open a dialog about it anyway.  If this is the case, we recommend opening an issue.

An issue is like a half way point between a support ticket and a forum post.  It's a public place for discussion, typically about some kind of suggestion to a project.  Like a support ticket, it can be marked "Open" or "Closed" depending on if it's been resolved, and can have people assigned to it.  To open an issue, navigate to our [GitLab Issue Page](https://gitlab.com/Alch_Emi/trig/-/issues/new), and fill out the title and description.  You'll need to create a GitLab account to do this.

Keep in mind that issues are more like suggestions than edits.  Make sure that you include information about where you want the custodians to put your writing, or why you wrote it.  A good issue might look something like this:

> Hey everyone!  I recently had a trip to the SHC and was told about how they can offer help with hormone injections.  I think we should add a paragraph to the section about the SHC on how to do this.  I wrote a quick paragraph that we could try putting into a new section after the "Available Services" section.
>
> > The SHC additionally offers walk-in appointments for students who want help with their hormone injections.  If you've been prescribed an injectable...
>
> I, Emi Simpson, have read and agree to the code of conduct for this project, and am willing to follow it for this and all future contributions.  I have also agree to license this paragraph under the Creative Commons Attribution Share-Alike 4.0 license.

You might notice the last sentence of this issue seems a bit odd.  For more information on what these things are and why we require them, see the section on [contributor agreements](#contributor-agreements).

After you create an issue, you'll be brought to it's issue page.  This is where you'll get any updates about your change, and where any discussion will take place about it.  You're welcome to save this page if you'd like, but you can always return to it later either way.

### Send an Email

If neither of the above options work for you, it's possible to submit a change by email.  This way of submitting a change isn't recommended, since it can't be seen and discussed by the community, but is made an option in order to ensure the projects accessibility.

As with an issue, make sure you include details about where and how you want your change inserted, as well as including an acknowledgement of the [contributor agreements](#contributor-agreements)

## How to use GitLab

When you first first land on the GitLab page, you'll see a lot of information.  It can be overwhelming at first, but it's actually pretty simple.  If you scroll down, you'll see a list of files.

![GitLab file browser UI](https://ipfs.io/ipfs/QmQWRV766e2mWtmTrizMXPpMZ2FzsL7CBFu1FyMs9kwLNS)

Most of these files provide infrastructure for the repository, as well as other minor settings.  The pages of the book are all within the `src/` directory, with three notable exceptions:  `CONTRIBUTING.md`, which corresponds to the page you're currently reading, and `LICENSE.md` and `CODE_OF_CONDUCT.md`, which are the files linked at the bottom of the sidebar.

Click on the `src/` directory and then any page to open up the viewer.  However, before you can edit it, you'll need to **fork** the repository.  This means that you'll have your own personal copy of the entire book that you can freely make edits to, and submit your changes when you're done.  When you click on the big blue "Edit" button, you'll be prompted if you want to fork the repository.  Click yes, and the repository will be forked, and you'll be brought to the editor.

> **Note:** If you're planning on editing multiple files, you may want to use the "Web IDE" button instead, which will bring you to a different editor, complete with file navigation.

![GitLab file viewer, with the "Edit" and "Fork" buttons highlighted](https://ipfs.io/ipfs/QmS9JQocHnbHrsiwLR3M4m2sh44yUJ1u3A519N3GYjV2CX)

Once you're in the editor, make any changes you need, then scroll down to the bottom of the page and click the green "Commit" button.  Web IDE users will be prompted to enter a commit message briefly summarizing your changes.  Something like "Added note about nurse visits to SHC page" is enough.  Feel free to leave the "Branch" or "Target Branch" field at its default value.  You will then be brought to the Merge Request page.  This is where you put your message to the custodians about what changes you made and why they should be merged.  Please fill out the "Title" and "Description" fields, but don't worry about any other fields.

If this is your first time contributing, please make sure you've read the section on this page about the Code of Conduct, as well as reading over the Contributor Covenant itself.  You'll find that section right after this one.

![A view of the Merge Request screen](https://ipfs.io/ipfs/QmXben6JGn9wxQeWhNy9kPkdDgyd6F5rqsGH5KyjV1FxwD)

Once you're ready to go, scroll to the bottom of the page and click the big green "Submit merge request" button and you'll be all set!  Don't worry about the contribution guidelines, that's what you're reading right now ^^

Finally, you'll be brought to your finished merge request page.  This is where any updates to your merge request will show up, including if it gets accepted, rejected, if someone requests a change, or if anyone comments!  You can get back to this page at any time by clicking the "Merge Requests" button on the sidebar, and finding your merge request in the list.

You'll also be able to see other people's issues and merge requests by clicking on the "Issues" and "Merge Requests" sidebar tabs.  Feel free to browse through and comment your feedback and input.  If you see an issue you think you can do, you can even add it to the project yourself using a merge request just like the one you just did!  Just make sure you link back to the original issue, and credit the original author if you add their writing to the project.

## Contributor Agreements

This project has two components that we ask you to agree to before contributing.  Before making any contribution to the project, please make sure you have read this section and understand what is expected of you.  If you do not, your contribution will not be accepted until you have included the proper acknowledgement, and you will be politely asked to read this section again.

### Code of Conduct: The Contributor Covenant

The first is [the contributor covenant][2], which is our code of conduct.  We strongly encourage you to read this document thoroughly.  It is the guidelines which the project curators hold the behavior of the community, and also a guide, should you ever need to report behavior.

Our goal is to make sure that the contributor covenant is acknowledged and known by all contributors, so that it can truly take effect.  Because of this, we ask all first-time contributors to include a statement of acknowledgement, seen below.

> I, *PREFERED NAME HERE*, have read and agree to the code of conduct for this project, and am willing to follow it for this and all future contributions.

This statement is not legally binding, and you are expected to follow the code of conduct whether or not you have given this statement.  There will be consequences to violating the code of conduct whether or not you've acknowledged it.  We ask you to acknowledge it only to ensure that all community members are familiar with it.

Note that this applies to you no matter how you're submitting your changes.  Merge requests, issues, and emails all need this acknowledgement if it's your first contribution.

### Project License

Additionally, we ask you to license your writing under the [Creative Commons Attribution Share-Alike 4.0 License][3].  This license gives us permission to redistribute your writing under the same license, but only if we give you proper credit.  This is the same license that this project as a whole is distributed under.

Specifically, the license grants permission to:

 - Distribute the work (with attribution, unless rescinded), such as by incorporating your writing in the published version of this project
 - Modify the work, to allow future writers make edits to sections you created or edited
 - Relicense the work under an equivalent license.  ("Equivalent Licenses" pretty much exclusively includes the Free Art License, and GPL v3.0, both with restriction only)

For more details, please read [the creative commons website][1].

When submitting a change using a merge request, your work is automatically licensed under the CC BY-SA 4.0 (it would need to be in order for you to be able to legally download and modify it).  However, when you're submitting writing using an issue or an email, you're technically writing from scratch, not modifying the project.  This means that your writing isn't automatically licensed under CC BY-SA 4.0.  Because of this, we ask for you to license your work before we can accept it, so that we can mix it with the current project and distribute it.  All you need to do is add a short sentence like, "I license the above writing under CC BY-SA 4.0, to be attributed to *YOUR PREFERED NAME*".  Note that the name you give here will be the name that you are publicly credited under, although you are welcome to update it later.

You may also give a personal link that you want included in your contribution, such as to your social media or website.  If you are contributing via merge request, please feel free to write yourself into the below contributors section!

## Contributors

We'd like to graciously recognize the invaluable contributions of the below contributors.

 - **Emi Simpson**, project founder.
 - No one else yet but please contribute :D

[1]: https://creativecommons.org/licenses/by-sa/4.0/
[2]: documents/covenant.md
[3]: documents/license.md
