#TRIG (Tiger's Resource for Identity and Gender)

[Introduction](intro.md)

- [Support](support/intro.md)
    - [Community & Counseling](support/groups.md)
    - [Discrimination, Harassment, & Sexual Assault](support/dhsa.md)
- [Social Transition](social/intro.md)
    - [Name Changes](social/name.md)
    - [Enforcing Pronouns](social/pronouns.md)
    - [Available Services](social/services.md)
- [Medical Transition](medical/intro.md)
    - [HRT at the SHC](medical/shc.md)
    - [Other Clinics in ROC](medical/other.md)
    - [Acquiring Letters](medical/letters.md)
    - [Insurance & Privacy](medical/insurance.md)
- [About](about.md)
    - [Contributing](contributing.md)

--------------

[License](documents/license.md)
[Contributor Covenant](documents/covenant.md)
