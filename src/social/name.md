# Name Changes

This section will detail the process of changing your name with RIT, including:

- How to initiate the process
- What additional steps might need to be taken in order to make sure it goes through
- What to do if your deadname still appears
- Special procedures for incoming students to update their username
- Taking steps to insure that your parents don't see your new name
