# Enforcing Pronouns

This section will detail:

- Your rights as an RIT student to the appropriate use of pronouns
- Where to get pronoun pins ^^
- The use of pronoun pins while in RIT's employ
- What to do if someone refuses to honor your pronouns
