# Available Services

This section will detail some of the available services at RIT that offer non-medical transition assistance.  For each service, the following details should be included:

- Name of the service
- Brief description of the service
- Where to find & how to sign up for the service
- Additional notes, concerns, and what to expect

At least the following services should be covered:

- Voice training with NTID
- Q Center binder swap
- Tangent clothing swap
