# Insurance & Privacy

This section will cover:
- Dealing with RIT's insurance
- Making sure your insurance doesn't out you
