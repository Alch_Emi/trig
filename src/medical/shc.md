# Informed Consent at the SHC

This section will provide:
- Details of the services provided at the SHC
- How to set up an appointment with the SHC
- What to expect on your first, second, and third visits
- The SHC provides assistance with hormone injections via nurse visits!
- A brief note about the existance of the committee that organizes HRT services at the SHC
