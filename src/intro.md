## ⚠️ This  document is moving to https://trig.alchemi.dev/ ⚠️

This copy is being left up for reference, but is most likely out of date.  Please see https://trig.alchemi.dev for the most up-to-date information.

<svg width=200 height=200 style="float:right">
    <use xlink:href="media/logo-min.svg#trig"></use>
</svg>

# Welcome to TRIG

Welcome to **TRIG**, the **T**iger's **R**esource for **I**dentity and **G**ender!  This is a living resource for any and all trans and non-binary students at RIT, documenting the rights and resources available to you at RIT and throughout the Rochester area.

Because people, policies, and groups are in constant flux, we don't believe that we can create a single, permanent guide to navigating life at RIT.  That's why this document aims to be constantly changing and always editable.  Everyone, including you, can chip in any information and advice that you want to share with the wider community.  If you want to help out, check out the [Contributing](contributing.md) section!

And as a final note, **this document was not meant for allies, parents, or faculty/staff.**  The target audience for this document is *exclusively* the transgender and non-binary students at RIT who are interested in and/or would benefit from knowing the details of transitioning and surviving at RIT.

- **If you are a parent** of a transgender or nonbinary student, a better resource for you would be [RIT's Q-Center][1], which is a central office for resources, clubs, and students in RIT's LGBTQIA+ community.  Please feel free to explore their website, and to reach out to them if you want to be put in touch with even more resources.  Additionally, you are encouraged to share this document with your child, so that they may benefit from it as well.

- **If you are an ally** looking to learn more about the state of trans/non-binary services on campus, I'd advise you to talk to the current spokesperson of the club [Tangent](mailto:transrit@gmail.com), who will be able to provide you up-to-date information from the perspective of current students, or that you speak with [the office of OUTspoken][3], the LGBTQIA+ advocacy branch of student government.

- **If you are cisgender but otherwise identify as LGBTQIA+**, you will likely not find much relevant information in this text.  If you are looking to get more involved with the LGBTQIA+ community at RIT, then [the QCenter's queer club directory][2] would be a great place to start.  If you are looking for a solution to a specific problem, it's the job of [OUTspoken][3] to advocate for you and provide you with resources, and they would be a great contact.

- **If you are not trans/non-binary, nor any of the above**, but have questions about this guide, it is currently being maintained by [Emi Simpson][4], although ownership will likely be transfered to OUTspoken or Tangent in the future.   If you have questions about the queer & LGBTQIA+ community at RIT, [the QCenter][1] (resources) or [OUTspoken][3] (advocacy) would be your best bet.

- **If you are a transgender and/or nonbinary student at RIT**, congratulations!  You're in the right place!  Please feel free to flick through these pages using the table of contents on the left, or by using the arrows at the bottom of the page.

### License
<p xmlns:dct="http://purl.org/dc/terms/" xmlns:cc="http://creativecommons.org/ns#" class="license-text"><a rel="cc:attributionURL" href="https://gitlab.com/Alch_Emi/trig"><span rel="dc:title">TRIG - Tiger's Resource for Identity & Gender</span></a> by <span rel="cc:attributionName">Emi Simpson & the listed contributors (see <a href="contributing.html">the "Contributing" page</a>) is licensed under </span><a href="https://creativecommons.org/licenses/by-sa/4.0" style="color: inherit">CC BY-SA 4.0
<svg width=22 height=22 viewBox="5.5 -3.5 64 64" style="vertical-align:text-bottom">
    <use xlink:href="media/cc.svg#cc" fill="currentColor"></use>
</svg>
<svg width=22 height=22 viewBox="5.5 -3.5 64 64" style="vertical-align:text-bottom">
    <use xlink:href="media/by.svg#by" fill="currentColor"></use>
</svg>
<svg width=22 height=22 viewBox="5.5 -3.5 64 64" style="vertical-align:text-bottom">
    <use xlink:href="media/sa.svg#sa" fill="currentColor"></use>
</svg>
</a></p>

Built using [mdBook](https://github.com/rust-lang/mdBook), which is available under [the Mozilla Public License v2.0](https://www.mozilla.org/MPL/2.0/)

[1]: https://rit.edu/qcenter
[2]: https://www.rit.edu/studentaffairs/qcenter/clubs.html
[3]: mailto:outspoken@rit.edu
[4]: mailto:emi@mail.rit.edu
[5]: https://creativecommons.org/licenses/by-sa/4.0/
