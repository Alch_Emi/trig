# Support Groups

This section will detail:

- Available trans & lgbtq clubs, including
    - Their purpose
    - How to find/contact/join them
    - The kinds of events each typically hold

- Available counseling option, such as the Q-Center's support group, CAPS, and 3rd party counselors & therapists
