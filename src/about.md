# About this Document

Thanks so much for reading through this book, and we hope you got what you came for!  These last two sections are "meta" sections, so to speak.  We'll be describing the details by which this document exists, and how to pitch in and contribute your knowledge and experiences to the document.  If that's not something you're interested in, you're welcome to stop reading here.  (Although we'd love it more than anything if you helped out!  Every experience is valid, and no tip is too small to be worth including)

This document is currently being maintained by Emi Simpson, although this is likely to change in the future.

## Why this book was created

Development of TRIG began the Spring semester of 2020, in response to the lack of a comprehensive and up-to-date guide to navigating life at RIT as a trans and/or non-binary student.  Much of the wisdom, tips, and advice was passed down by word of mouth, in 1-to-1 settings, or in group settings like Tangent that only a fraction of the trans community attended.

Prior to the creation of TRIG, there existed a singular document for trans students at RIT: A pamphlet, titled Trans@RIT, created by the Q center (currently available [on the q-center website][1], with a [mirror stored here][2]).  Unfortunately, a large portion of the information in this guide is out of date or no longer relevant, yet this was the best available resource.  TRIG was created to supersede the Trans@RIT pamphlet by collecting the word-of-mouth experiences of students into a central, permanent location.

## Why this book is hosted the way it is

GitLab may seem like an odd choice for hosting what is basically just a wiki, and to be fair, it kinda is.  However, this document in particular has some special considerations that must be taken into consideration, and for now, it is believed that GitLab is the best option going into the future.

Ultimately, **this document was built to survive**.  A dead resource is no use to anyone, so it is important to make sure that this text doesn't go the way of Trans@RIT.  This meant not only that it needed to remain available to access, but also to contribute.  The specific criteria that went into this decision are:

- **This document must be able to survive financial hardship.**  There are many really cool solutions to hosting wikis and live-documents, but unfortunately, many of them cost money.  The downside to this is that if a student misses a payment, or a club decides to stop funding the project, everything disappears.  There are a *lot* of events that could cause a recurring payment to be missed, and the risk of this text being destroyed was deemed too great to use any paid service.

- **This document must be able to survive it's maintainers suddenly leaving.**  The social landscape is in constant flux.  Students come and go, clubs form and are disbanded, projects are forgotten.  It's not safe to assume that any custodian of any project will continue to be able to maintain that project or pass it off successfully.  This means that if someone finds this text abandoned, they need to be able to bring it back.  GitLab enables this by way of forking.  Even if the owner of the repository disappears, all forks continue to work, and can be hosted, therefor preserving the knowledge and allowing contributions once more.  It is also for this reason that this document should not be hosted on a student-owned machine

- **This document must be able to survive malicous parties.**  Unfortunately, not everyone accepts trans identities, and there may be attempts to sabotage or defile this document.  Therefor, this document needs to be able to survive malicious edits, or RIT deciding that it doesn't actually want this guide to be hosted on one of its servers.  The former case is handled by the fact that changes to the document need to be manually approved by the project maintainers, and the latter case is handled by hosting the project out of RIT's control.

- **This document must be able to be contributed to by anyone.**  If not everyone can contribute to this document, then not everyone's experiences can be collected, and TRIG will have failed it's purpose.  This is where GitLab falls somewhat short, since not everyone may be familiar with git.  However, GitLab does offer an in-browser editor, and this document seeks to outline multiple ways to contribute information, including by Merge Request, Issue, or Email, such that there is a means to contribute for anyone.  See [the contributing section](contributing.md) for more info.

## Contingencies

There are a number of cases that might arise wherein a major change to the storage, distribution, or infrastructure of this document might need to take place.  In order to facilitate a smooth transition, this section will attempt to predict and provide plans for as many of these contingencies as possible.

### The Maintainer of this document cannot be reached

If the maintainer or maintainers of this document cannot be contacted for an extended period of time, or if the student group responsible for the custodianship of this document is disbanded/drops the project, then:

1. If a club was the previous maintainer of the document, then the club center should be contacted to attempt to retrieve the email of the club, allowing access to the repository.  It is then possible to transfer ownership of the repository to another trusted group.  Note:  If the group maintaining this document is Tangent, OUTspoken should be met with for email recovery instead of the club center.
2. If the repository cannot be recovered, then it should be forked by the new custodian.
3. A merge request should be submitted to the origin repository tracking the master branch of the new repository, and with a note that custodianship of the document has been transfered to the new repository.
4. A note should be added to the "history of this document" section noting this event
5. A deployment system should be re-configured.
6. The Q-center and all other interested parties should be informed of the change.

### The project is to be moved off of GitLab

If this project is to be moved off of GitLab due to consensus of either the custodial group or a large number of contributors, then:

1. The GitLab should **remain available**, as many links might still point to that location
2. A note should be added to the "Introduction" section of the GitLab version indicating that the text has been moved, and where to find it.
3. The "Why this book is hosted the way it is" section above should be updated to reflect the new rational, to prevent inform future custodians of the pitfalls experienced with the previous implementation, and why this new implementation was chosen (and hence why they shouldn't move it again)
4. All other mentions of the GitLab should be updated or removed, except where relevant for historical documentation.
5. The "Contributing" section should be re-written to clearly provide several *accessable* ways to contribute
6. Steps should be taken to ensure that the text cannot be easily lost.  This might include public backups, periodic data dumps, providing recovery details to trusted agents (as well as documenting this so that others know who to talk to), and any other measures deemed appropriate for the specific platform being used.
7. The "history of this document" section should be updated to reflect this change

### Transfer of Ownership

There will likely come a time where the current custodian of this document will step down or pass on custodianship to another willing party.  When this occurs:

1. The receiving party should have complete control over the repository.  Not just access but ownership.  This ensures that further transfers of ownership are possible
2. A backup fork of the repository should be kept by the old maintainer, just in case.
3. If the URL of the project changes, the Q-center and all other interested parties should be notified
4. Additionally, links in the repository to the gitlab need to be updated as well, including...
   - Any links in the contributing page
   - The link in the license on the intro page
   - The repository link in `book.toml` (which appears at the top of every page)
5. The following references to the current custodian need to be updated:
   - The introduction page
   - The top of this page
   - The contributor covenant
4. The "history of this document" section should be updated to reflect this change

### No custodians are available

It is possible that a custodian needs to step down, but no other potential custodians are willing to take on the project.  If this occurs:

1. A GitLab account in the email of OUTspoken or the Q-Center should be created.
2. Ownership of the repository should be transfered to this account.  The recieving party will not be responsible for custodianship, but will be expected to vet any potential new custodians.  The "Transfer of Ownership" protocol should be used for this.
3. A prominent note should be added to the "Introduction" section indicating that a maintain is needed, and provide the contact details of the party named above.
4. If a new custodian approaches the party holding the repository, they should be briefly vetted (just to ensure they have no bad intent), and the "Transfer of Ownership" protocol above should be used to give them custodianship.
5. The "history of this document" section should be updated to reflect that this process took place.

## History of this document

- TRIG was created Spring semester for 2020 by Emi Simpson, who remains the current maintainer of the project.

*Note: This section will be expanded as time goes on*

[1]: https://www.rit.edu/studentaffairs/qcenter/resources/TransAtRitGuide.pdf
[2]: https://ipfs.io/ipfs/QmYQkBaq3V9HXudyDBD7F4tiHyAoZcbJEZr5CKQTp5QVc7/TransAtRIT.pdf
