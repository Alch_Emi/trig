# TRIG

**TRIG**, or the **T**iger's **R**esource for **I**dentity and **G**ender
is an ever-evolving collection of knowledge and advice for transgender and
nonbinary students at Rochester Institute of Technology.  Our goal is to collect
information on the rights and resources available to students at RIT into a
central location.

In order to capture as much information as possible, and to make the guide
relevant to as many students as we can, this project is open to contributions by
anyone who wants to help share information.  For more information on how to
contribute, please see the "contributing" section.

Much more information is available inside the book itself, so please [jump right
in!][1]

Except where noted, all content here is licensed under [CC BY-SA 4.0][2], and was authored by Emi Simpson and the contributors listed in the "Contributing" section.

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/80x15.png" /></a>

[1]: https://alch_emi.gitlab.io/trig/
[2]: https://creativecommons.org/licenses/by-sa/4.0/
